import collections
import csv
import itertools
import multiprocessing
import operator
import re
from collections import OrderedDict
from datetime import datetime, timezone, timedelta
from functools import partial
from multiprocessing import Process

from Scripts import chkcsv
from skcriteria import Data, MIN, MAX
import isodate

from mysql import connector
from mysql.connector import errorcode

import pandas as pd
from skcriteria.madm import simple
from textblob import TextBlob

config = {
    'host': 'youtubedataserver2.mysql.database.azure.com',
    'user': 'twilson20',
    'password': 'ccXxDlHlF4',
    'database': 'youtubedatadatabase',
    'client_flags': [connector.ClientFlag.SSL],
    'ssl_ca': '../static/files/DigiCertGlobalRootG2.crt.pem'
}
connection = None
cursor = None
try:
    connection = connector.connect(**config)
    print("Connection established")
except connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with the user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)
else:
    cursor = connection.cursor()

def readcsv(filename):
    channels = []
    with open('static/files/' + filename) as f:
        reader = csv.reader(f)
        next(reader, None)
        for row in reader:
            channels.append(row[0])
    return channels


def getvideodata(video):
    videoid = video[0]
    dur = isodate.parse_duration(video[8])
    latest_comment = ''
    num_comments = 0
    num_likes = 0
    num_views = 0
    if video[10] != None:
        latest_comment = video[10].strftime("%d/%m/%Y, %H:%M:%S")
    else:
        latest_comment = '0000-00-00 00:00:00'
    video_detail = [video[1],[("title",video[2]),
                              ("videoUrl", video[7]),
                              ("description", video[6]),
                              ("numComments", video[4]),
                              ("numLikes", video[5]),
                              ("numViews", video[3]),
                              ("duration", str(dur)),
                              ("numPositiveComments", video[11]),
                              ("latestComment", latest_comment),
                              ("uploaddate", video[9].strftime("%d/%m/%Y, %H:%M:%S")),
                              ("videoid", videoid)
                              ]]
    days_from_upload = datetime.now() - video[9]
    if days_from_upload.days == 0:
        days_from_upload = timedelta(days=1)
    days_from_latest_comment = timedelta(days=0)
    if video[10] is not None:
        days_from_latest_comment = datetime.now() - video[10]
    if video[4] is not None:
        num_comments = video[4]
    if video[5] is not None:
        num_likes = video[5]
    if video[3] is not None:
        num_views = video[3]
    rank_detail = [video[1], [video[2], round(int(num_comments) / days_from_upload.days),
                              round(int(num_likes) / days_from_upload.days),
                              round(int(num_views) / days_from_upload.days), round(dur.total_seconds()),
                              days_from_latest_comment.days, video[11]]]
    return video_detail, rank_detail


def getcommentdata(comment):
    commentid = comment[1]
    uploaddate = ''
    if comment[4] is not None:
        uploaddate = comment[4].strftime("%d/%m/%Y, %H:%M:%S")
    text = re.sub(r'[^\w]', ' ', comment[2])
    polarity = TextBlob(text).sentiment.polarity
    comment_detail = {"commentid": commentid,
                      "videoid": comment[0],
                      "comment": comment[2],
                      "likes": comment[3],
                      "uploaddate": uploaddate,
                      "polarity": polarity
                      }
    days_from_upload = datetime.now() - comment[4]
    if days_from_upload.days == 0:
        days_from_upload = timedelta(days=1)

    rank_detail = [commentid, round(int(comment[3]) / days_from_upload.days), polarity]
    return comment_detail, rank_detail

def getallChannels():
    query = "SELECT * FROM `channeldata`"
    cursor.execute(query)
    data = cursor.fetchall()
    return data

def getallvideodata():
    video_details = []
    rank_details = []
    query = "SELECT * FROM `videodata` ORDER BY `channel_id`"
    cursor.execute(query)
    data = cursor.fetchall()
    for video in data:
        video_detail, rank_detail = getvideodata(video)
        video_details.append(video_detail)
        rank_details.append(rank_detail)
    return video_details, rank_details


def getallvideodatabychannel(channelid):
    video_details = []
    rank_details = []
    format_strings = ",".join("%s" for _ in channelid)
    query = "SELECT * FROM `videodata` WHERE channel_id in (%s) ORDER BY `channel_id`" % format_strings
    cursor.execute(query, channelid)
    data = cursor.fetchall()
    for video in data:
        video_detail, rank_detail = getvideodata(video)
        video_details.append(video_detail)
        rank_details.append(rank_detail)
    return video_details, rank_details


def getallcommentdata(video_id, channelid):
    comment_details = []
    rank_details = []
    channelid = channelid + 'comments'
    query = "SELECT * FROM `{tablename}` WHERE video_id = %s".format(tablename=channelid)
    cursor.execute(query, (video_id,))
    data = cursor.fetchall()
    for comment in data:
        video_detail, rank_detail = getcommentdata(comment)
        comment_details.append(video_detail)
        rank_details.append(rank_detail)
    return comment_details, rank_details


def getvideobyid(video_id):
    query = "SELECT * FROM `videodata` WHERE video_id = %s"
    cursor.execute(query, (video_id,))
    data = cursor.fetchone()
    return data


def calculateweights(ranks):
    weights = []
    for rank in ranks:
        n_rank = (2 * (len(ranks) + 1 - rank)) / (pow(len(ranks), 2) + len(ranks))
        weights.append(n_rank)
    return weights


def getvideoweightedproductrank(video_details, allvideos):
    video_info = ['Title', 'Comment Count', 'Like Count', 'View Count', 'Duration', 'Latest Comment',
                  'Positive Comments']
    ranks = [4, 1, 3, 6, 5, 2]
    weights = calculateweights(ranks)
    video_copy = video_details.copy()
    video_copy.insert(0, video_info)
    video_copy = pd.DataFrame(video_copy[1:], columns=video_copy[0])
    video_copy = video_copy.loc[:, video_info].head(
        len(video_details))
    copy = video_copy.copy()
    video_copy = Data(video_copy.iloc[:, 1:], [MAX, MAX, MAX, MIN, MIN, MAX], anames=video_copy['Title'],
                      cnames=video_copy.columns[1:], weights=weights)
    dm = simple.WeightedProduct(mnorm="sum")
    dec = dm.decide(video_copy)
    copy.loc[:, 'rank'] = dec.rank_
    x = 0
    return copy.to_dict('records')


def getcommentweightedproductrank(comment_details):
    comment_info = ['Comment ID', 'Like Count', 'Polarity']
    ranks = [1, 2]
    weights = calculateweights(ranks)
    comment_copy = comment_details.copy()
    comment_copy.insert(0, comment_info)
    comment_copy = pd.DataFrame(comment_copy[1:], columns=comment_copy[0])
    comment_copy = comment_copy.loc[:, comment_info].head(
        len(comment_details))
    copy = comment_copy.copy()
    video_copy = Data(comment_copy.iloc[:, 1:], [MAX, MAX], anames=comment_copy['Comment ID'],
                      cnames=comment_copy.columns[1:], weights=weights)
    dm = simple.WeightedProduct(mnorm="sum")
    dec = dm.decide(video_copy)
    copy.loc[:, 'rank'] = dec.rank_
    return copy.to_dict('records')


def groupbychannelid(details):
    dict = {}
    for elem in details:
        if elem[0] not in dict:
            dict[elem[0]] = []
        dict[elem[0]].extend(elem[1:])
    return dict


def convertlisttodict(details):
    requiredDict = {}
    for detail in details:
        requiredDict[detail] = []
        values = details[detail]
        for item in values:
            dict = {}
            for i in range(0, 11):
                dict[item[i][0]] = item[i][1]
            if detail=='AllVideos':
                dict[item[12][0]]=item[12][1]
            else:
                dict[item[11][0]]=item[11][1]
            requiredDict[detail].append(dict)
    return requiredDict


def tupletodict(allvideos):
    vdict = {}
    for l in allvideos:
        vdict[l[0]] = []
        for i in l[1]:
            vdict[l[0]].extend(i)
    return vdict

if __name__ == '__main__':
    csvchecker = chkcsv.read_format_specs('youtubelink.fmt', True, True,)
    check = chkcsv.check_csv_file("static/files/youtubelink.csv", csvchecker, False, True, False, False)
    if len(check) == 0:
        channelids=[]
        channels = readcsv("youtubelink.csv")
        for channel in channels:
            i = str(channel).rfind("/")
            channelid = str(channel)[i + 1:]
            channelids.append(channelid)
        channelsdict={}
        channelslist = getallChannels()
        for channel in channelslist:
            channelsdict[channel[0]] = channel[1]
        details, rank = getallvideodatabychannel(channelids)
        allvideos = []
        allranks = []
        for i in details:
            allvideos.append(i[1])
        for k in rank:
            allranks.append(k[1])
        allvideoranks = getvideoweightedproductrank(allranks, allvideos)
        allvlist = groupbychannelid(details)
        allvlist['AllVideos'] = allvideos
        videos = groupbychannelid(details)
        ranks = groupbychannelid(rank)
        channelranks=[]
        for i in videos:
            channelranks.append(getvideoweightedproductrank(ranks[i], videos[i]))
        channelranks.append(allvideoranks)
        r = 0
        for i in allvlist:
            x = 0
            for vlist in allvlist[i]:
                rank = ('rank',channelranks[r][x]['rank'])
                vlist.append(rank)
                x += 1
            r += 1
        allvlist = convertlisttodict(allvlist)
        print(allvlist)
