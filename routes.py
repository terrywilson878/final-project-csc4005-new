import os
import json
import datetime
from collections import OrderedDict
from io import TextIOWrapper

from Scripts import chkcsv
from werkzeug.utils import secure_filename

from flask import Flask, render_template, request, redirect, url_for, session, jsonify, flash

import youtubedata

app = Flask(__name__)
app.secret_key = "super secret key"

messages = [{'title': 'Message One',
             'content': 'Message One Content'},
            {'title': 'Message Two',
             'content': 'Message Two Content'}
            ]

UPLOAD_FOLDER = 'static/files'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
all_videos = {}


@app.route('/')
def sayHello():
    csvchecker = chkcsv.read_format_specs('youtubelink.fmt', True, True,)
    check = chkcsv.check_csv_file("static/files/youtubelink.csv", csvchecker, False, True, False, False)
    if len(check) == 0:
        channelids=[]
        channels = youtubedata.readcsv("youtubelink.csv")
        for channel in channels:
            i = str(channel).rfind("/")
            channelid = str(channel)[i + 1:]
            channelids.append(channelid)
        channelsdict={}
        channelslist = youtubedata.getallChannels()
        for channel in channelslist:
            channelsdict[channel[0]] = channel[1]
        details, rank = youtubedata.getallvideodatabychannel(channelids)
        allvideos = []
        allranks = []
        for i in details:
            allvideos.append(i[1])
        for k in rank:
            allranks.append(k[1])
        allvideoranks = youtubedata.getvideoweightedproductrank(allranks, allvideos)
        allvlist = youtubedata.groupbychannelid(details)
        allvlist["AllVideos"] = allvideos
        videos = youtubedata.groupbychannelid(details)
        ranks = youtubedata.groupbychannelid(rank)
        channelranks=[]
        for i in videos:
            channelranks.append(youtubedata.getvideoweightedproductrank(ranks[i], videos[i]))
        channelranks.append(allvideoranks)
        r = 0
        for i in allvlist:
            x = 0
            for vlist in allvlist[i]:
                rank = ("rank",channelranks[r][x]["rank"])
                vlist.append(rank)
                x += 1
            r += 1
        allvlist = youtubedata.convertlisttodict(allvlist)
        global all_videos
        all_videos = allvlist
        return redirect(url_for("multiplevideo", channels=json.dumps(channelsdict)))
        #return jsonify(all_videos)


@app.route('/multiplevideo', methods=["GET", "POST"])
def multiplevideo():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        channels = request.args['channels']
        channels = json.loads(channels)
    return render_template('MultipleVideosNew.html', videos=all_videos, channels=channels)

@app.route('/getchannelvideos')
def channelvideos():
    id = request.args['id'].replace('table', '')
    data = all_videos[id]
    return json.dumps({"data": data})

if __name__ == "__main__":
    app.run(host="localhost", port=8008,debug=True)
